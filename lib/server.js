var fs = require("fs");
var http = require("http");
var path = require("path");
var url = require("url");
var fs = require("fs");
var WebSocketServer = require("ws").Server;

var INTERVAL_BASE = 250;
var TEAMS_ROUTE = /^\/teams\/?$/;
var PUBLIC_PATH = path.join(process.cwd(), 'public');
console.log('PUBLIC_PATH', PUBLIC_PATH);

/**
 * @param {Array} arr
 * @return {Array}
 */
var copy = function(arr) {
    return arr.concat();
};

/**
 * @param {String} fileName
 */
var readFile = function(fileName) {
    return fs.readFileSync(
        path.resolve(
            path.join(__dirname, "..", "data", fileName)
        )
    );
};

var games = JSON.parse(readFile("games.json"));
var teams = JSON.parse(readFile("teams.json"));

var server = http.createServer(function(req, res) {

    if (TEAMS_ROUTE.test(req.url)) {
        console.log("[%s] GET /teams", new Date().toISOString());

        var teamData = JSON.stringify(teams);

        res.writeHead(200, {
            "Content-Type": "application/json",
            "Content-Length": teamData.length
        });

        res.write(teamData);
        res.end();
    } else {

        var uri = url.parse(req.url).pathname,
            filename;

        uri = uri.replace(/\.\./g, '');
        filename = path.join(PUBLIC_PATH, uri);

        console.log('filename', filename)

        fs.exists(filename, function(exists) {

          if(!exists) {
            res.writeHead(404, {"Content-Type": "text/plain"});
            res.write("404 Not Found\n");
            res.end();
            return true;
          }

          if (fs.statSync(filename).isDirectory()){
            filename += '/index.html';
          }

          fs.readFile(filename, "binary", function(err, file) {
            if(err) {
              res.writeHead(500, {"Content-Type": "text/plain"});
              res.write(err + "\n");
              res.end();
              return true;
            }

            res.writeHead(200);
            res.write(file, "binary");
            res.end();
            return true;
          });
        });
    }


});

var app = new WebSocketServer({
    server: server,
    path: "/games"
});

app.on("connection", function(socket) {
    console.log("[%s] /games stream opened", new Date().toISOString());

    var queue = copy(games);

    var loop = setTimeout(function tick() {
        socket.send(JSON.stringify(queue.shift()));

        if (queue.length) {
            loop = setTimeout(tick, Math.random() * INTERVAL_BASE | 0);
        }
    }, Math.random() * INTERVAL_BASE | 0);

    socket.on("close", function() {
        console.log("[%s] /games stream closed", new Date().toISOString());
        clearTimeout(loop);
    });
});

/**
 * @param {Number} port
 * @param {String} host
 */
var listen = exports.listen = function(port, host) {
    server.listen(port, host, function() {
        var info = server.address();
        console.log("Server listening on http://%s:%d", info.address, info.port);
    });
};

if (require.main === module) {
    listen(
        process.env.PORT || 9090,
        process.env.HOST || "localhost"
    );
}
