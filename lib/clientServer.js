
var connect = require('connect'),
    serveStatic = require('serve-static'),
    path = require('path'),
    app,
    public;

public = path.resolve(__dirname, '..', 'public');

app = connect();
app.use(serveStatic(public, {'index': ['index.html']}));
app.listen(9091);

console.log('client server listening to', 9091);
