
var Utils = {};

Utils.dateStringToTimestamp = function(date){

  if(typeof date !== 'string' || date.split('/').length !== 3){
    return NaN;
  }

  var dateParts = date.split('/');

  //  we assume all dates are in 2000+
  return (new Date('20'+dateParts[2], dateParts[1], dateParts[0])).getTime();
};


//  a simple id generator
Utils.generateId = (function(){

  var seed = 1;

  return function(){
    seed += 1;
    return seed;
  };
})();

module.exports = Utils;
