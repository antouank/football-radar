var keyMirror = require('keymirror');

module.exports = keyMirror({
  ADD_TEAM_TO_LEAGUE: null,
  ADD_RESULT_TO_LEAGUE: null,
  RESET_LEAGUE_STATE: null,
  TEAM_INFO_OPEN: null,
  TEAM_INFO_CLOSE: null,
  TEAM_INFO_SET_TEAM_ID: null
});
