
/** @jsx React.DOM */
var React = require('react'),
    teamsService = require('./services/teamsService'),
    resultsService = require('./services/resultsService'),
    MainApp = require('./components/MainApp.jsx'),
    app;


//  load the teams
teamsService
.loadTeams(function(){

  // then start getting results
  resultsService.start();
});


app = (function() {

  return React
  .render(
    <MainApp />,
    document.querySelector('#main-app')
  );

})();
