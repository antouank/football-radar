'use strict';

var ResultsService = {},
    Utils = require('../common/Utils'),
    AppActions = require('../actions/AppActions'),
    ws,
    gamesSocketAddress = 'ws://'+location.host+'/games',
    handleMessages;

//  a handler for the socket messages
handleMessages = function handleMessages(msg){

  if(msg.data === undefined){
    throw new Error('got message without data!');
  }

  var result;

  try {
    result = JSON.parse(msg.data)
  } catch (e){
    throw new Error('message data is not in JSON format!');
  }

  result.date = Utils.dateStringToTimestamp(result.date);

  //  normalize goals number
  result.homeGoals = +result.homeGoals;
  result.awayGoals = +result.awayGoals;

  AppActions.addResult(result);
};


ResultsService.start = function(){

  ws = new WebSocket(gamesSocketAddress);

  ws.onmessage = handleMessages;
};


module.exports = ResultsService;
