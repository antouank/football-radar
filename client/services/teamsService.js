
var http = require('http'),
    AppActions = require('../actions/AppActions'),
    teamsService = {},
    options;


options = {
  path: '/teams',
  method: 'GET'
};


teamsService.loadTeams = function(cb){

  var req =
  http
  .request(options, function(res){

    if(res.statusCode !== 200){
      throw new Error('could not get teams!');
    }

    var body = '';

    res.on('data', function(chunk){

      body += chunk;
    });

    res.on('end', function(){

        //  we expect a JSON response
        var jsonData = JSON.parse(body);

        jsonData
        .forEach(function(teamObject){

          AppActions.addTeam(teamObject.id, teamObject.name);
        });

        cb();
    });

  });

  req.end();

};

module.exports = teamsService;
