
var AppDispatcher = require('../dispatcher/AppDispatcher'),
    EventEmitter = require('events').EventEmitter,
    AppConstants = require('../constants/AppConstants'),
    assign = require('object-assign'),
    CHANGE_EVENT = 'change',
    //  Map of the team
    _teamInfoState = {
      open: false,
      teamId: undefined
    };



//  ===================================================== UiStore
var UiStore = assign({}, EventEmitter.prototype, {

  /**
  * Get the timestamp of the latest result in the league
  * @return {number} date timestamp
  */
  getTeamInfoState: function(){
    return _teamInfoState;
  },

  /**
  * Emit a 'change' event to all the listeners
  */
  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  /**
  * @param {function} callback
  */
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  /**
  * @param {function} callback
  */
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }
});


// Register to handle all updates
AppDispatcher.register(function(payload) {

  var action = payload.action,
      //  flag to let us know if there is a change in this store
      seenChange = true;

  switch(action.actionType) {

    case AppConstants.TEAM_INFO_OPEN:

      _teamInfoState.open = true;
      break;

    case AppConstants.TEAM_INFO_CLOSE:

      _teamInfoState.open = false;
      break;

    case AppConstants.TEAM_INFO_SET_TEAM_ID:

      _teamInfoState.teamId = action.teamId;
      break;

    default:
      seenChange = false;
      return true;
  }


  //  update the UI
  seenChange && UiStore.emitChange();

  return true; // No errors.  Needed by promise in Dispatcher.
});

module.exports = UiStore;
