
var AppDispatcher = require('../dispatcher/AppDispatcher'),
    EventEmitter = require('events').EventEmitter,
    AppConstants = require('../constants/AppConstants'),
    assign = require('object-assign'),
    lazy = require('lazy.js'),
    CHANGE_EVENT = 'change',
    //  Map of the team,
    _teams = {},
    _results = [],
    _lastResultDate = 0,
    _leagueMeta,
    _cachedTable;


//  define the league's meta information
_leagueMeta = {
  name: 'Premier League 2011/2012',
  properties: [
    'position',
    'name',
    'goalsFor',
    'goalsAgainst',
    'points'
  ]
};

/**
* Create a Team item.
* @param  {number} id     The id of the team ( handle error? )
* @param  {string} name   The name of the team
*/
function addTeam(id, name) {

  _teams[id] = {
    id: id,
    name: name,
    points: 0,
    goalsFor: 0,
    goalsAgainst: 0,
    position: 0,
    positionHistory: [],
    pointsHistory: [],
    goalsForHistory: []
  };
}

/**
* Add a result
* @param  {result} a Result object
* contains:
*   date: {number} date of the result as a timestamp
*   homeTeamId: {number} id of the home team
*   awayTeamId: {number} id of the away team
*   homeTeamGoals: {number} goals of the home team
*   awayTeamGoals: {number} goals of the away team
*
*/
function addResult(result){

  if(
    result === undefined ||
    isNaN(result.homeGoals) ||
    isNaN(result.awayGoals) ||
    _teams[result.homeTeamId] === undefined ||
    _teams[result.awayTeamId] === undefined
  ){
    throw new Error('result is invalid!');
  }

  var teamHome,
      teamAway;

  //  keep references to the teams
  teamHome = _teams[result.homeTeamId];
  teamAway = _teams[result.awayTeamId];

  //  store the result
  _results.push(result);

  // store the latest data
  if(result.date > _lastResultDate){
    _lastResultDate = result.date;
  }

  //  add the points
  if(result.homeGoals === result.awayGoals){
    teamHome.points += 1;
    teamAway.points += 1;
  }
  else if(result.homeGoals > result.awayGoals){
    teamHome.points += 3;
  }
  else if(result.homeGoals < result.awayGoals){
    teamAway.points += 3;
  }

  // keep the points in the history
  teamHome.pointsHistory.push(teamHome.points);
  teamAway.pointsHistory.push(teamAway.points);

  // add the 'for' goals
  teamHome.goalsFor += result.homeGoals;
  teamAway.goalsFor += result.awayGoals;

  //  keep em in history
  teamHome.goalsForHistory.push(teamHome.goalsFor);
  teamAway.goalsForHistory.push(teamAway.goalsFor);

  // remove the 'against' goals
  teamHome.goalsAgainst -= result.awayGoals;
  teamAway.goalsAgainst -= result.homeGoals;

  //  calculate the positions and cache the sorted table
  _cachedTable = sortByPoints(_teams);

  //  store the position in the array
  teamHome.positionHistory.push(teamHome.position);
  teamAway.positionHistory.push(teamAway.position);
}

/**
* Reset the points of all the teams
*/
function resetPoints() {

  Object.keys(_teams)
  .forEach(function(teamId){
    _teams[teamId].points = 0;
  });
}

function sortByPoints(teamsMap){

  var sortedMap =
  lazy(teamsMap)
  .values()
  .sort(function(teamObjA, teamObjB){

    //  sort by points difference
    if(teamObjA.points < teamObjB.points){
      return 1;
    }
    else if(teamObjA.points > teamObjB.points){
      return -1;
    }
    //   if they have the same points, sort by goals difference
    else if(
      (teamObjA.goalsFor + teamObjA.goalsAgainst) < (teamObjB.goalsFor + teamObjB.goalsAgainst)
    ){
      return 1;
    }
    else if(
      (teamObjA.goalsFor + teamObjA.goalsAgainst) > (teamObjB.goalsFor + teamObjB.goalsAgainst)
    ){
      return -1;
    }
    // if they have the same goals difference, sort by absolute goalsFor number
    else if(teamObjA.goalsFor < teamObjB.goalsFor){
      return 1;
    }
    else if(teamObjA.goalsFor > teamObjB.goalsFor){
      return -1;
    }
    //  last is Name ordering
    else if(teamObjA.name < teamObjB.name){
      return 1;
    }
    else if(teamObjA.name > teamObjB.name){
      return -1;
    }
  })
  .toArray();

  sortedMap
  .forEach(function(teamObj, i){
    teamObj.position = i + 1;
  });

  return sortedMap;
}



//  ===================================================== LeagueStore
var LeagueStore = assign({}, EventEmitter.prototype, {

  /**
  * Get all the teams
  * @param {number} the id of the team
  * @return {object} the team found
  */
  getTeamById: function(id) {
    return lazy(_teams)
    .find(function(teamObj){
      return teamObj.id === id;
    });
  },

  /**
  * Get the league table
  * @return {array}
  */
  getTable: function() {
    return _cachedTable;
  },

  /**
  * Get the meta info for the league
  * @return {object}
  */
  getMeta: function(){
    return _leagueMeta;
  },

  /**
  * Get the timestamp of the latest result in the league
  * @return {number} date timestamp
  */
  getLastResultDate: function(){
    return _lastResultDate;
  },

  /**
  * Emit a 'change' event to all the listeners
  */
  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  /**
  * @param {function} callback
  */
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  /**
  * @param {function} callback
  */
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }
});


// Register to handle all updates
AppDispatcher.register(function(payload) {

  var action = payload.action,
      //  flag to let us know if there is a change in this store
      seenChange = true;

  switch(action.actionType) {

    //  add a new team to this league
    case AppConstants.ADD_TEAM_TO_LEAGUE:

      addTeam(action.id, action.name);
      break;

    //  add a new result to this league
    case AppConstants.ADD_RESULT_TO_LEAGUE:

      addResult(action.result);
      break;

    default:
      seenChange = false;
      return true;
  }


  

  //  update the UI
  seenChange && LeagueStore.emitChange();

  return true; // No errors.  Needed by promise in Dispatcher.
});

module.exports = LeagueStore;
