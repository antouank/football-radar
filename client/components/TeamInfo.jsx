
/** @jsx React.DOM */
var React = require('react'),
    LeagueStore = require('../stores/LeagueStore'),
    AppActions = require('../actions/AppActions'),
    BarChart = require('../components/BarChart.jsx'),
    TeamInfo;


TeamInfo = React.createClass({

  /**
  * Return a state for this component, based on the props given, or
  * the props of this component
  * @param  {object} props  The props to use
  * @return {object} the state of this component
  */
  getState: function(props){

    var state = {},
        propsToUse = props,
        teamObject;

    if(propsToUse === undefined){
      propsToUse = this.props;
    }

    teamObject = LeagueStore.getTeamById(propsToUse.teamId);

    state.open = propsToUse.open;

    if(state.open === true && teamObject !== undefined){
      state.team = teamObject;
    }

    return state;
  },


  getInitialState: function() {
    return this.getState();
  },


  closeIconHandler: function() {
    AppActions.closeTeamInfo();
  },


  componentDidMount: function() {
    LeagueStore.addChangeListener(this._onChange);
  },


  componentWillReceiveProps: function(nextProps){

    if(nextProps.open === true){
      this.setState(this.getState(nextProps));
    }
  },


  constructGraphPart: function(){
  
    return (
      <div className="seven columns hide-less-than-tablet">
        <div className="bar-chart-container">
          <BarChart
            data={this.state.team.goalsForHistory}
            minVal={0}
            maxVal={93}
            color={'#4CAF50'}
          />
          <BarChart
            data={this.state.team.positionHistory}
            minVal={20}
            maxVal={1}
            color={'#727272'}
          />
        </div>
      </div>
    );
  },


  render: function() {

    var containerClassName = 'team-info-container row paper-container',
        teamDetailsPart = '',
        graphPart = '',
        gamesPlayed,
        avgGoalsFor,
        avgGoalsAgainst;

    if(this.props.open === true){
      containerClassName += ' open';
    } else {
      containerClassName += ' closed';
    }

    // construct the internal part
    if(this.state.team !== undefined){

      gamesPlayed = this.state.team.positionHistory.length;
      avgGoalsFor = (this.state.team.goalsFor/gamesPlayed).toFixed(1);
      avgGoalsAgainst = (this.state.team.goalsAgainst/gamesPlayed).toFixed(1);

      teamDetailsPart = (
        <div className="four columns team-details">

          <div className="row team-name">
            {this.state.team.name}
          </div>

          <div className="row team-position">
            <div className="six columns">
              Position:
            </div>
            <div className="six columns">
              {this.state.team.position}
            </div>
          </div>

          <div className="row team-position">
            <div className="six columns">
              Points:
            </div>
            <div className="six columns">
              {this.state.team.points}
            </div>
          </div>

          <div className="row team-goals">
            <div className="six columns">
              Goals
            </div>
            <div className="three columns">
              <span className="goals-for">{this.state.team.goalsFor}</span>
            </div>
            <div className="three columns">
              <span className="goals-against">{this.state.team.goalsAgainst}</span>
            </div>
          </div>
          
          <div className="row team-goals">
            <div className="six columns">
              Goals/game
            </div>
            <div className="three columns">
              <span className="goals-for"> {avgGoalsFor} </span>
            </div>
            <div className="three columns">
              <span className="goals-against"> {avgGoalsAgainst} </span>
            </div>
          </div>
        </div>
      );

      graphPart = this.constructGraphPart();
    }

    return (
      <div className={containerClassName}>
        <div className="twelve columns">
          <div className="team-info">
            <div className="row">
              {teamDetailsPart}
              {graphPart}
            </div>
          </div>
        </div>
        <div className="icon icon-close" onClick={this.closeIconHandler}>
          X
        </div>
      </div>
    );
  },

  _onChange: function() {

    if(this.props.open === true){
      this.setState(this.getState());
    }
  }

});

module.exports = TeamInfo;
