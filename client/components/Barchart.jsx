/** @jsx React.DOM */
var React = require('react'),
    d3 = require('d3');

/**
 *  A sample bar chart to use in the TeamInfo component
 *  some hardcoded values included, just for demonstration purposes
 */


//============================== <Bar />
var Bar = React.createClass({

  getDefaultProps: function() {
    return {
      width: 0,
      height: 0
    }
  },

  render: function() {
    return (
      <rect 
        fill={this.props.color}
        width={this.props.width + '%'}
        // height={this.props.height}
        height={'2px'}
        x={(this.props.index * this.props.width) + '%'}
        y={this.props.availableHeight - this.props.height} />
    );
  }
});

//============================== <DataSeries />
var DataSeries = React.createClass({

  getDefaultProps: function() {
    return {
      title: '',
      data: []
    }
  },

  render: function() {
    var props = this.props;

    var barWidth = 100/this.props.data.length;

    var yScale = d3.scale.linear()
      .domain([this.props.minY, this.props.maxY])
      .range([0, this.props.height]);

    var xScale = d3.scale.ordinal()
      .domain(d3.range(this.props.data.length))
      .rangeRoundBands([0, barWidth], 0.05);

    var bars = this.props.data
    .map(function(point, i) {
      return (
        <Bar 
          height={yScale(point)}
          width={barWidth}
          index={i}
          availableHeight={props.height}
          color={props.color}
          key={i} />
      )
    });

    return (
      <g>{bars}</g>
    );
  }
});

//============================== <Chart />
var Chart = React.createClass({
  render: function() {

    return (
      <div className="chart">
        <svg width='100%' height={this.props.height}>{this.props.children}</svg>
      </div>
    );
  }
});

//============================== <BarChart />
var BarChart = React.createClass({

  getDefaultProps: function() {
    return {
      height: 220,
      data: []
    }
  },

  render: function() {

    //  min and max values are hardcoded for this demo example

    return (
      <Chart height={this.props.height}>
        <DataSeries
          data={this.props.data}
          minY={this.props.minVal}
          maxY={this.props.maxVal}
          height={this.props.height}
          color={this.props.color} />
      </Chart>
    );
  }
});

module.exports = BarChart;
