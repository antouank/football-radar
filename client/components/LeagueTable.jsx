
/** @jsx React.DOM */
var React = require('react'),
    Utils = require('../common/Utils'),
    AppActions = require('../actions/AppActions'),
    LeagueTable,
    hideProperty;

/**
 * @param {string} proprty
 * @return {boolean}
 *
 *  We "hardcode" a function that can tell us whether a property can bt
 *  hidden for UI reasons or not ( for example in a small view )
 */
hideProperty = function(property){

  var propertiesNotHidden = [
    'name',
    'position',
    'points'
  ];

  return propertiesNotHidden.indexOf(property) === -1;
};


//  ========================================  <LeagueTable />
LeagueTable = React.createClass({

  //  a handler for clicking on a row
  //  in our demo, it only opens the 'teaminfo' component,
  //  and sets the team id to the one that the row has
  rowClickHandler: function(teamId){

    return function(ev, reactId){

      AppActions.setTeamInfoTeamId(teamId);
      AppActions.openTeamInfo();
    };
  },

  render: function(){

    var headers,
        rows,
        self = this,
        leagueTable = self.props.leagueTable,
        leagueProperties = self.props.leagueProperties;

    //  if no league table data exists, return nothing
    if(leagueTable === undefined){
      return (<div></div>);
    }


    //  construct the headers based on the properties given
    headers =
    leagueProperties
    .map(function makeTableHeaders(property){
      
      var classString = '';

      if(hideProperty(property)){
        classString = 'hide-less-than-tablet';
      }

      return <th className={classString}>{property}</th>;
    });


    //  construct the rows based on the team values
    rows =
    leagueTable
    .map(function forEveryTeamObject(teamObj){

      var properties,
          thisTeamId;

      thisTeamId = teamObj.id;

      properties =
      leagueProperties
      .map(function makeTableCells(property){

        var classString = '';

        //  if this property can be hidden, add a class to hide it
        //  in views that are small
        if(hideProperty(property)){
          classString = 'hide-less-than-tablet';
        }

        return (
          <td data-property-name={property} className={classString}>
            {teamObj[property]}
          </td>
        );
      });

      return (
        <tr key={teamObj.position} onClick={self.rowClickHandler(thisTeamId)}>
          {properties}
        </tr>
      );
    });


    //  --------------------------------  render
    return (
      <div className="league-table">
        <table>
          <thead>
            <tr>
              {headers}
            </tr>
          </thead>
          <tbody>
            {rows}
          </tbody>
        </table>
      </div>
    );
  }
});

module.exports = LeagueTable;
