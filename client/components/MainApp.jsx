
/** @jsx React.DOM */
var React = require('react'),
    LeagueStore = require('../stores/LeagueStore'),
    UiStore = require('../stores/UiStore'),
    LeagueTable = require('../components/LeagueTable.jsx'),
    TeamInfo = require('../components/TeamInfo.jsx'),
    MainApp,
    getMainAppState;

/**
  * Return a state for the Main App component
  *
  * @return {object} the state of this component
  */
getMainAppState = function(){

  //  fecth the current state of the UI Store
  var teamInfoState = UiStore.getTeamInfoState();

  return {
    leagueTable: LeagueStore.getTable(),
    leagueMeta: LeagueStore.getMeta(),
    latestDate: LeagueStore.getLastResultDate(),
    teamInfoOpen: teamInfoState.open,
    teamInfoTeamId: teamInfoState.teamId
  };
};


MainApp = React.createClass({

  getInitialState: function() {
    return getMainAppState();
  },

  //  when the component is mounted
  componentDidMount: function() {

    // register to listen for changes
    //  from the league store, for changes in the league table data
    LeagueStore.addChangeListener(this._onChange);
    //  and the UI store, to listen to changes to the UI state
    UiStore.addChangeListener(this._onChange);
  },


  render: function() {

    //  a row to display the date of the last change in the league table
    var dateRow = '';

    //  if there is a valid date timestamp
    if(this.state.latestDate > 0){
      //  create the element
      dateRow = (
        <div className="twelve columns latest-date-info">
          {'league table as of: ' + (new Date(this.state.latestDate)).toDateString()}
        </div>
      );
    }

    return (
      <div className="container">
        <div className="top-header">
          <span className="header-title">{this.state.leagueMeta.name}</span>
        </div>
        <TeamInfo open={this.state.teamInfoOpen} teamId={this.state.teamInfoTeamId} />
        <div className="row paper-container">
          {dateRow}
          <div className="twelve columns">
            <LeagueTable
              leagueTable={this.state.leagueTable}
              leagueProperties={this.state.leagueMeta.properties} />
          </div>
        </div>
      </div>
    );
  },

  _onChange: function() {
    this.setState( getMainAppState() );
  }

});

module.exports = MainApp;
