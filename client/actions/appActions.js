
var AppDispatcher = require('../dispatcher/AppDispatcher');
var AppConstants = require('../constants/AppConstants');

var AppActions = {

  /**
  * @param  {number} number
  * @param  {string} team name
  */
  addTeam: function(id, name) {
    AppDispatcher.handleServerAction({
      actionType: AppConstants.ADD_TEAM_TO_LEAGUE,
      id: id,
      name: name
    });
  },

  /**
  * @param  {result} a Result object
  * contains:
  *   date: {number} date of the result as a timestamp
  *   homeTeamId: {number} id of the home team
  *   awayTeamId: {number} id of the away team
  *   homeTeamGoals: {number} goals of the home team
  *   awayTeamGoals: {number} goals of the away team
  *
  */
  addResult: function(result) {

    AppDispatcher.handleServerAction({
      actionType: AppConstants.ADD_RESULT_TO_LEAGUE,
      result: result
    });
  },

  openTeamInfo: function() {

    AppDispatcher.handleViewAction({
      actionType: AppConstants.TEAM_INFO_OPEN
    });
  },

  closeTeamInfo: function() {

    AppDispatcher.handleViewAction({
      actionType: AppConstants.TEAM_INFO_CLOSE
    });
  },

  /**
  * @param  {number} the team id
  */
  setTeamInfoTeamId: function(teamId) {

    AppDispatcher.handleViewAction({
      actionType: AppConstants.TEAM_INFO_SET_TEAM_ID,
      teamId: teamId
    });
  }
};

module.exports = AppActions;
