var should = require('should'),
    AppDispatcher = require('../../dispatcher/AppDispatcher'),
    AppConstants = require('../../constants/AppConstants'),
    UiStore;

//  monkeypatch the AppDispatcher
AppDispatcher.register = function(cb){

  if(AppDispatcher.callbacks === undefined){
    AppDispatcher.callbacks = [];
  }

  AppDispatcher.callbacks.push(cb);
};


AppDispatcher.triggerAction = function(action){

  if(Array.isArray(AppDispatcher.callbacks)){

    AppDispatcher.callbacks
    .forEach(function(cb){

      cb.call(null, { action: action });
    });
  }

  return this;
};


describe('UiStore', function(){

  before(function(){
    UiStore = require('../../stores/UiStore');
  });


  describe('.getTeamInfoState()', function(){

    it('should be a Function', function(){
      (UiStore.getTeamInfoState).should.be.a.Function;
    });

    it('should return the state of the TeamInfo section', function(){

      var state = UiStore.getTeamInfoState();
      state.should.be.an.Object;
    });

  });

});
