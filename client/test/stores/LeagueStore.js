var should = require('should'),
    AppDispatcher = require('../../dispatcher/AppDispatcher'),
    AppConstants = require('../../constants/AppConstants'),
    LeagueStore;

//  monkeypatch the AppDispatcher
AppDispatcher.register = function(cb){

  if(AppDispatcher.callbacks === undefined){
    AppDispatcher.callbacks = [];
  }

  AppDispatcher.callbacks.push(cb);
};


AppDispatcher.triggerAction = function(action){

  if(Array.isArray(AppDispatcher.callbacks)){

    AppDispatcher.callbacks
    .forEach(function(cb){

      cb.call(null, { action: action });
    });
  }

  return this;
};


describe('LeagueStore', function(){

  before(function(){
    LeagueStore = require('../../stores/LeagueStore');
  });


  describe('.getTable()', function(){

    it('should be a Function', function(){
      (LeagueStore.getTable).should.be.a.Function;
    });

    it('should return a table with two teams we added', function(done){

      var firstTime = true,
          secondTime = false,
          spy;

      spy = function(){
        var table = LeagueStore.getTable();
        (table).should.be.an.Array;

        //  first team is added
        if(firstTime === true){

          (table.length).should.equal(1);
          firstTime = false;
          secondTime = true;
          return true;
        }
        //  second team added
        else if(secondTime === true){

          (table.length).should.equal(2);
          LeagueStore.removeChangeListener(spy);
        }

        done();
      };

      LeagueStore.addChangeListener(spy);

      //  let's add two teams
      AppDispatcher
      .triggerAction({
        actionType: AppConstants.ADD_TEAM_TO_LEAGUE,
        id: 1,
        name: 'Foo FC'
      })
      .triggerAction({
        actionType: AppConstants.ADD_TEAM_TO_LEAGUE,
        id: 2,
        name: 'Bar FC'
      });

    });


    it('should return a table with correct points and positions', function(done){

      var firstTime = true,
          secondTime = false,
          spy;
      
      spy = function(){

        var table = LeagueStore.getTable();
        (table).should.be.an.Array;
        (table.length).should.equal(2);

        //  first result is added
        if(firstTime === true){

          firstTime = false;
          secondTime = true;
          return true;
        }

        //  second result added
        
        (table[0].id).should.equal(2);
        (table[0].goalsFor).should.equal(2);
        (table[0].goalsAgainst).should.equal(-1);
        (table[0].position).should.equal(1);
        (table[0].points).should.equal(4);

        (table[1].id).should.equal(1);
        (table[1].goalsFor).should.equal(1);
        (table[1].goalsAgainst).should.equal(-2);
        (table[1].position).should.equal(2);
        (table[1].points).should.equal(1);

        LeagueStore.removeChangeListener(spy);
        done();
      };

      LeagueStore.addChangeListener(spy);

      //  let's add two teams
      AppDispatcher
      .triggerAction({
        actionType: AppConstants.ADD_RESULT_TO_LEAGUE,
        result: {
          homeTeamId: 1,
          awayTeamId: 2,
          homeGoals: 1,
          awayGoals: 1,
          date: Date.now()
        }
      })
      .triggerAction({
        actionType: AppConstants.ADD_RESULT_TO_LEAGUE,
        result: {
          homeTeamId: 1,
          awayTeamId: 2,
          homeGoals: 0,
          awayGoals: 1,
          date: Date.now()
        }
      });

    });
  });



  describe('.getMeta()', function(){

    it('should be a Function', function(){
      (LeagueStore.getMeta).should.be.a.Function;
    });

    it('should return meta information for the league', function(){

        var meta = LeagueStore.getMeta();

        (meta).should.be.an.Object;
        (meta.name).should.be.a.String;
        (meta.properties).should.be.an.Array;
    });

  });


  describe('.getTeamById()', function(){

    it('should be a Function', function(){
      (LeagueStore.getTeamById).should.be.a.Function;
    });

    it('should return the team object for that id', function(){

      var teamObject = LeagueStore.getTeamById(1);

      (teamObject).should.be.an.Object;
      (teamObject.id).should.equal(1);
      (teamObject.name).should.be.a.String;
      (teamObject.points).should.be.a.Number;
    });

    it('should return undefined if the team is not found', function(){

      var teamObject = LeagueStore.getTeamById(111);

      should(teamObject).equal(undefined);
    });

  });

});
